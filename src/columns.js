export const columns = [
  {
    id: 1,
    Header: "First Name",
    accessor: "first_name", // accessor is the "key" in the data
  },
  {
    id: 2,
    Header: "Last Name",
    accessor: "last_name",
  },
  {
    id: 3,
    Header: "Location",
    accessor: "location",
  },
];

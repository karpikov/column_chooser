import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

import Main from "./Main";

ReactDOM.render(<Main />, document.getElementById("root"));

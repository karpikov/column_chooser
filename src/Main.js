import React, { useState } from "react";
import ColumnChooser from "./ColumnChooser/Main";
import DataTable from "./DataTable";
import { columns } from "./columns";

function Main() {
  const selectedColumnIds = [1];
  const [selectedColumns, setSelectedColumns] = useState(
    columns.filter((column) => selectedColumnIds.includes(column.id))
  );

  const changeSelectedColumns = (ids) => {
    setSelectedColumns(
      columns
        .filter((column) => ids.includes(column.id))
        .sort((a, b) => ids.indexOf(a.id) - ids.indexOf(b.id))
    );
  };

  return (
    <div className="Container">
      <ColumnChooser
        columns={columns}
        selectedIds={selectedColumnIds}
        changeSelectedColumns={changeSelectedColumns}
      />
      <DataTable columns={selectedColumns} />
    </div>
  );
}

export default Main;

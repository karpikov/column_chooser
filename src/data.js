export const data = [
  {
    first_name: "John",
    last_name: "Smith",
    location: "Poland",
  },
  {
    first_name: "Tom",
    last_name: "Jerry",
    location: "USA",
  },
  {
    first_name: "Jane",
    last_name: "Walker",
    location: "Ukraine",
  },
];

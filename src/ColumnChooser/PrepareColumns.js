import Reac from "react";
import { Icon, IconSize } from "@blueprintjs/core";

export function addSortAction(array, onSort) {
  const move = (oldIndex, type) => {
    let arr = [...array];
    let newIndex = type == "up" ? oldIndex - 1 : oldIndex + 1;

    while (newIndex < 0) {
      newIndex += arr.length;
    }
    if (newIndex >= arr.length) {
      var k = newIndex - arr.length;
      while (k-- + 1) {
        arr.push(undefined);
      }
    }
    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
    return onSort(arr.map((element) => element.id));
  };

  return array.map((element, index) => {
    element.secondaryLabel = (
      <>
        <a onClick={() => move(index, "up")}>
          <Icon icon="arrow-up" size={15} />
        </a>
        <a onClick={() => move(index, "dowm")}>
          <Icon icon="arrow-down" size={15} />
        </a>
      </>
    );

    return element;
  });
}

export function removeSortAction(array) {
  return array.map((element, index) => {
    element.secondaryLabel = null;

    return element;
  });
}

export function prapareColumns(columns) {
  return columns.map((item) => {
    return {
      id: item.id,
      //label: item.Header,
      label: item.Header,
      hasCaret: false,
      isSelected: false,
    };
  });
}

export function prapareSelectedColumns(columns, onSort) {
  const move = (oldIndex, type) => {
    let arr = [...columns];
    let newIndex = type == "up" ? oldIndex - 1 : oldIndex + 1;

    if (newIndex < 0) {
      let element = arr.shift();
      return onSort([...arr.map((element) => element.id), element.id]);
    }
    if (newIndex >= arr.length) {
      let element = arr.pop();
      return onSort([element.id, ...arr.map((element) => element.id)]);
    }

    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
    return onSort(arr.map((element) => element.id));
  };

  return columns.map((item, index) => {
    return {
      id: item.id,
      //label: item.Header,
      label: item.Header,
      hasCaret: false,
      isSelected: false,
      secondaryLabel: (
        <>
          <a onClick={() => move(index, "up")}>
            <Icon icon="arrow-up" size={15} />
          </a>
          <a onClick={() => move(index, "dowm")}>
            <Icon icon="arrow-down" size={15} />
          </a>
        </>
      ),
    };
  });
}

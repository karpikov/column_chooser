import React from "react";
import { Tree, Classes } from "@blueprintjs/core";

function Columns({ columns, handleClick }) {
  return (
    <Tree
      className={`${Classes.ELEVATION_0} column-chooser-block`}
      contents={columns}
      onNodeClick={handleClick}
    />
  );
}

export default Columns;

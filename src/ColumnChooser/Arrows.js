import React from "react";
import { Icon, IconSize } from "@blueprintjs/core";

function Arrow({ selectColumns, removeColumns }) {
  return (
    <div className="col-1">
      <div className="vertical-align">
        <a onClick={selectColumns}>
          <Icon icon="arrow-right" size={20} />
        </a>
        <br />
        <a onClick={removeColumns}>
          <Icon className="mt-3" icon="arrow-left" size={20} />
        </a>
      </div>
    </div>
  );
}

export default Arrow;

import React from "react";
import { Classes, Button } from "@blueprintjs/core";

function Footer({ closeSettings, saveSelectedColumns }) {
  return (
    <div className={Classes.DIALOG_FOOTER}>
      <div className={Classes.DIALOG_FOOTER_ACTIONS}>
        <Button className={Classes.DIALOG_CLOSE_BUTTON} onClick={closeSettings}>
          Cancel
        </Button>
        <Button className="Classes.BUTTON" onClick={saveSelectedColumns}>
          OK
        </Button>
      </div>
    </div>
  );
}

export default Footer;

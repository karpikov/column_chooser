import React from "react";
import { Icon } from "@blueprintjs/core";

function Settings({ openSettings }) {
  return (
    <>
      <div className="row mt-2 mb-2">
        <div className="col-12 text-right">
          <a onClick={openSettings}>
            <Icon icon="settings" size={20} />
          </a>
        </div>
      </div>
    </>
  );
}

export default Settings;

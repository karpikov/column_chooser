import React, { useState, useEffect } from "react";
import { Classes, Dialog, Button, Tree, Intent, Card } from "@blueprintjs/core";
import {
  prapareColumns,
  prapareSelectedColumns,
  addSortAction,
  removeSortAction,
} from "./PrepareColumns";
import Settings from "./Settings";
import Arrows from "./Arrows";
import Footer from "./Footer";
import Columns from "./Columns";

function ColumnChooser({ columns, selectedIds, changeSelectedColumns }) {
  const sortedColumnsClick = (sortedIds) => {
    setSelectedColumnsIds([...sortedIds]);
  };

  const [isOpen, setIsOpen] = useState(false);
  const openSettings = () => setIsOpen(true);
  const closeSettings = () => setIsOpen(false);

  const [availableColumns, setAvailableColumns] = useState([]);
  const [selectedColumns, setSelectedColumns] = useState([]);
  const [selectedColumnsIds, setSelectedColumnsIds] = useState(selectedIds);

  useEffect(() => {
    setAvailableColumns(
      prapareColumns(
        columns.filter((column) => !selectedColumnsIds.includes(column.id))
      )
    );
    setSelectedColumns(
      prapareSelectedColumns(
        columns
          .filter((column) => selectedColumnsIds.includes(column.id))
          .sort(
            (a, b) =>
              selectedColumnsIds.indexOf(a.id) -
              selectedColumnsIds.indexOf(b.id)
          ),
        sortedColumnsClick
      )
    );
  }, [selectedColumnsIds]);

  const saveSelectedColumns = () => {
    changeSelectedColumns(selectedColumnsIds);
    closeSettings();
  };

  const selectColumns = () => {
    setSelectedColumnsIds([
      ...selectedColumnsIds,
      ...availableColumns
        .filter((element) => element.isSelected)
        .map((element) => element.id),
    ]);
  };

  const removeColumns = () => {
    setSelectedColumnsIds([
      ...selectedColumns
        .filter((element) => !element.isSelected)
        .map((element) => element.id),
    ]);
  };

  const handleNodeClick = (item) => {
    setAvailableColumns([
      ...availableColumns.map((element) => {
        if (element.id == item.id) {
          element.isSelected = !element.isSelected;
        }
        return element;
      }),
    ]);
  };

  const handleSelectedNodeClick = (item) => {
    setSelectedColumns([
      ...selectedColumns.map((element) => {
        if (element.id == item.id) {
          element.isSelected = !element.isSelected;
        }
        return element;
      }),
    ]);
  };

  return (
    <>
      <Settings openSettings={openSettings} />
      <Dialog
        className={Classes.DIALOG}
        title="Column Chooser"
        isOpen={isOpen}
        autoFocus={true}
        canEscapeKeyClose={true}
        canOutsideClickClose={true}
        enforceFocus={true}
        usePortal={true}
        onClose={closeSettings}
        style={{ width: 0.5 * window.innerWidth }}
      >
        <Card>
          <div className="row mt-2 mb-2">
            <div className="col-6">
              <h4 className="bp4-heading mb-2">Available Columns</h4>
              <Columns
                columns={availableColumns}
                handleClick={handleNodeClick}
              />
            </div>
            <Arrows
              selectColumns={selectColumns}
              removeColumns={removeColumns}
            />
            <div className="col-5">
              <h4 className="bp4-heading mb-2">Selected Columns</h4>
              <Columns
                columns={selectedColumns}
                handleClick={handleSelectedNodeClick}
              />
            </div>
          </div>
        </Card>

        <Footer
          closeSettings={closeSettings}
          saveSelectedColumns={saveSelectedColumns}
        />
      </Dialog>
    </>
  );
}

export default ColumnChooser;
